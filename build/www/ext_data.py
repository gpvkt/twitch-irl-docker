#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    twitch-irl-docker
    Copyright (C) 2022  gpkvt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import obs_settings
import cgitb
import memcache
import json

if obs_settings.debug:
    cgitb.enable()

print("HTTP/1.0 200 OK")
print("Content-type:application/json;charset=utf-8\r\n")

mc = memcache.Client(['localhost:11211'], debug=0)

if obs_settings.debug:
    with open('debug.json') as json_file:
        data = json.dumps(json.load(json_file))
        mc.set("ESP32", data)

ext_data = mc.get("ESP32")
print(ext_data)
