#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    twitch-irl-docker
    Copyright (C) 2022  gpkvt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import obs_settings
import cgi, cgitb
import memcache
import time

from obswebsocket import obsws, requests
from pprint import pprint

if obs_settings.debug:
    cgitb.enable()

form = cgi.FieldStorage()
cmd = form.getvalue('cmd')
action = form.getvalue('action')

host = obs_settings.host
port = obs_settings.port
password = obs_settings.password

ws = obsws(host, port, password)
mc = memcache.Client(['localhost:11211'], debug=0)

try:
    ws.connect()
except:
    print("Could not connect to OBS")

if cmd == "stream":
    if action == "start":
        mc.set('OBS_STREAMING_STATUS', "True")
        ws.call(requests.StartStreaming())
    else:
        mc.set('OBS_STREAMING_STATUS', "False")
        ws.call(requests.StopStreaming())

if cmd == "scene":
    ws.call(requests.SetCurrentScene(action))

if cmd == "audio":
    ws.call(requests.ToggleMute(action))

if cmd == "source":
    if 'filter|' in action:
        p = action.split('|')
        source = p[1]
        source_filter = p[2]
        toggle = p[3]
        if toggle == '1':
            toggle = True
        else:
            toggle = False
        ws.call(requests.SetSourceFilterVisibility(source, source_filter, toggle))
    else:
        ws.call(requests.SetSceneItemRender(action,False))
        ws.call(requests.SetSceneItemRender(action,True))

print("HTTP/1.0 200 OK")
print("Content-type:text/html;charset=utf-8\r\n")
time.sleep(2)
