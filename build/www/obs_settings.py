#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

host = os.environ.get('OBS_URL', 'host.docker.internal')
port = os.environ.get('OBS_PORT', '4444')
password = os.environ.get('OBS_PASSWORD', False)
dayflat = os.environ.get('DAYFLAT', False)
liveu = os.environ.get('LIVEU', False)
offline_scene = os.environ.get('OBS_OFFLINE_SCENE', 'OFFLINE')
channel = os.environ.get('IRC_CHANNEL', False)
debug = os.environ.get('DEBUG', False)
chat_height = os.environ.get('CHAT_HEIGHT', 500)
extdata = os.environ.get('EXTDATA', False)
tts = os.environ.get('TTS', False)
tts_url = os.environ.get('TTS_URL', 'http://localhost/tts')
twitch_parent = os.environ.get('TWITCH_CHAT_PARENT', 'localhost')
