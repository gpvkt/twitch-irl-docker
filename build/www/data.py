#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    twitch-irl-docker
    Copyright (C) 2022  gpkvt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import obs_settings
import cgitb
import memcache
import string
import json

from pprint import pprint

if obs_settings.debug:
    cgitb.enable()

safe = string.ascii_letters + string.digits

def safeid(s):
    s = ''.join([c for c in s if c in safe])
    while not s[0] in string.c:
        s = s[1:]
    return str(s)

def check_mc_value_float(value):
    if value == None:
        return 0
    else:
        return float(value)

def check_mc_value_bool(value):
    if value:
        return "True"
    else:
        return "False"

def check_mc_value_string(value):
    if value is None:
        return "False"
    else:
        return str(value)

print("HTTP/1.0 200 OK")
print("Content-type:application/json;charset=utf-8\r\n")

mc = memcache.Client(['localhost:11211'], debug=0)

dayflat = check_mc_value_bool(obs_settings.dayflat)

dropped_frames = check_mc_value_float(mc.get('OBS_NUM-DROPPED-FRAMES'))
uptime = check_mc_value_float(mc.get('OBS_STREAM-TIMECODE'))
cpu_usage = check_mc_value_float(mc.get('OBS_CPU-USAGE'))
bitrate_out = check_mc_value_float(mc.get('OBS_KBITS-PER-SEC'))
bitrate_in = check_mc_value_float(mc.get('RTMP_BITRATE'))
dropped_connections = check_mc_value_float(mc.get('RTMP_NACCEPTED'))
battery_connected = check_mc_value_bool(mc.get('LIVEU_BATTERY_CONNECTED'))
battery_charging = check_mc_value_bool(mc.get('LIVEU_BATTERY_CHARGING'))
battery_discharging = check_mc_value_bool(mc.get('LIVEU_BATTERY_DISCHARGING'))
battery_percentage = check_mc_value_float(mc.get('LIVEU_BATTERY_PERCENTAGE'))
battery_time = check_mc_value_float(mc.get('LIVEU_BATTERY_RUNTIMETOEMPTY'))
liveu = check_mc_value_bool(mc.get('LIVEU'))

current_scene = check_mc_value_string(mc.get('OBS_CURRENT_SCENE'))
streaming_status = check_mc_value_string(mc.get('OBS_STREAMING_STATUS'))
obs_status = check_mc_value_string(mc.get('OBS_CONNECTED'))

low_bitrate = check_mc_value_bool(mc.get('RTMP_LOW_BITRATE'))

active_vlc_sources = check_mc_value_string(mc.get('OBS_ACTIVE_VLC_SOURCES'))
active_filters = check_mc_value_string(mc.get('OBS_ACTIVE_FILTERS'))
active_audio = check_mc_value_string(mc.get('OBS_ACTIVE_AUDIO')) 
muted_vlc_sources = check_mc_value_string(mc.get('OBS_MUTED_VLC_SOURCES'))
muted_filters = check_mc_value_string(mc.get('OBS_MUTED_FILTERS'))
muted_audio = check_mc_value_string(mc.get('OBS_MUTED_AUDIO'))

connection = ""
service = ""
if mc.get('ESP32') is not None:
    ext_data = json.loads(mc.get('ESP32'))
    if 'Connection' in ext_data:
        connection = ext_data['Connection']
    if 'Service' in ext_data:
        service = ext_data['Service']

print('{"numdroppedframes": "%s", "totalstreamtime": "%s", "cpuusage": "%s", "kbitspersec": "%s", "bitrate": "%s", "connections": "%s", "batteryconnected": "%s", "batterycharging": "%s", "batterydischarging": "%s", "batterypercentage": "%s", "batterytime": "%s", "liveu": "%s", "currentscene": "%s", "activevlcsources": "%s", "activefilters": "%s", "activeaudio": "%s", "mutedvlcsources": "%s", "mutedfilters": "%s", "mutedaudio": "%s", "streamingstatus": "%s", "obsstatus": "%s", "lowbitrate": "%s", "dayflat": "%s", "connection": "%s %s"}' % (dropped_frames, uptime, cpu_usage, bitrate_out, bitrate_in, dropped_connections, battery_connected, battery_charging, battery_discharging, battery_percentage, battery_time, liveu, current_scene, active_vlc_sources, active_filters, active_audio, muted_vlc_sources, muted_filters, muted_audio, streaming_status, obs_status, low_bitrate, dayflat, service, connection))
