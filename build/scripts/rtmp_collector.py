#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    twitch-irl-docker
    Copyright (C) 2022  gpkvt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import time
import logging
import memcache

from pprint import pprint
from lib import config
from lib import rtmp

def main():
    conf = config.get_config()
    r = rtmp.RtmpStats(conf)

    # Must be AFTER import config, as config sets default logging options
    logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s: %(message)s')
    logging.getLogger().setLevel(conf['LOG_LEVEL'])

    logging.debug('Connecting to memcache')
    mc = memcache.Client(['localhost:11211'], debug=0)

    while True:
        try:
            rtmp_stats = r.get()
            stream = conf['RTMP_STREAM_NAME']

            mc.set("RTMP_ERROR", r.error())
            mc.set("RTMP_PUBLISHING", r.get_publishing_status())
            mc.set("RTMP_BITRATE", r.get_bitrate())
            mc.set("RTMP_LOW_BITRATE", r.check_low_bitrate())

            while not r.get_publishing_status():
                logging.warning('RTMP isn\'t publishing, waiting for incoming stream...')
                mc.set("RTMP_LOW_BITRATE", True)
                mc.set("RTMP_BITRATE", 0)
                time.sleep(1)
                continue

            last_reconnect = int(rtmp_stats[stream]['client'][0]['time']) / 1000
            mc.set("RTMP_UPTIME", rtmp_stats['uptime'])
            mc.set("RTMP_LAST_RECONNECT", str(last_reconnect))
            mc.set("RTMP_NACCEPTED", rtmp_stats['accepted'])
            mc.set("RTMP_BYTES_IN", rtmp_stats[stream]['bytes_in'])
            mc.set("RTMP_BYTES_IN_SUM", rtmp_stats['bytes_in_sum'])

            logging.info('-------------------------------------------------------------------------------')
            logging.info('RTMP Status')
            logging.info("Monitored Stream     : %s", str(stream))
            logging.info("Publishing           : %s", str(mc.get('RTMP_PUBLISHING')))
            logging.info("Bitrate              : %s", str(mc.get('RTMP_BITRATE')))
            logging.info("Uptime (Total)       : %s", str(mc.get('RTMP_UPTIME')))
            logging.info("Uptime (Stream)      : %s", str(mc.get('RTMP_LAST_RECONNECT')))
            logging.info("Accepted             : %s", str(mc.get('RTMP_NACCEPTED')))
            logging.info("Bytes In (Stream)    : %s", str(mc.get('RTMP_BYTES_IN')))
            logging.info("Bytes In (Total)     : %s", str(mc.get('RTMP_BYTES_IN_SUM')))
            logging.info("Error                : %s", str(mc.get('RTMP_ERROR')))
            logging.info('-------------------------------------------------------------------------------')
        except Exception as e:
            logging.error('Error getting values from RTMP')
            logging.info(e)
            time.sleep(10)

        time.sleep(1)

if __name__ == "__main__":
    main()
