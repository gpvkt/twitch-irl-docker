#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    twitch-irl-docker
    Copyright (C) 2022  gpkvt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging

logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s: %(message)s')

vars = [
  'OBS_URL',
  'OBS_PORT',
  'OBS_PASSWORD',
  'OBS_START_SCENE',
  'OBS_LIVE_SCENE',
  'OBS_BRB_SCENE',
  'OBS_FORCED_BRB_SCENE',
  'OBS_OFFLINE_SCENE',
  'LOW_BITRATE',
  'RTMP_STAT_URL',
  'RTMP_STREAM_NAME',
  'LIVEU',
  'LIVEU_TOKEN_TTL',
  'LIVEU_USERNAME',
  'LIVEU_PASSWORD',
  'LIVEU_URL',
  'LIVEU_UNIT',
  'LIVEU_API',
  'IRC_CHANNEL',
  'IRC_USERNAME',
  'IRC_OAUTH_TOKEN',
  'IRC_COOLDOWN',
  'IRC_GREETER_WELCOME',
  'IRC_GREETER_EMOTE',
  'IRC_BOT_EMOTE',
  'IRC_BITRATE_MSG_PRE',
  'IRC_BITRATE_MSG_SUF',
  'IRC_LOW_BITRATE_MSG_INTERVAL',
  'PUSHOVER_USER_KEY',
  'PUSHOVER_APP_TOKEN',
  'LOG_LEVEL',
  'DAYFLAT',
  'CHAT_HEIGHT',
  'DEBUG',
  'EXTDATA',
  'TTS',
  'TTS_URL',
  'TWITCH_CHAT_PARENT'
]

def get_config():
    ''' Get config values from environment, see `examples/docker-compose/docker-compose.override.yml` for details '''
    conf = {}

    for var in vars:
        if var not in os.environ:
            conf[var] = False
        else:
            conf[var] = os.environ.get(var)

    if not conf['LOG_LEVEL']:
        logging.getLogger().setLevel(logging.ERROR)
        logging.warning('LOG_LEVEL is not set, using default')
        conf['LOG_LEVEL'] = logging.ERROR
    else:
        logging.info('Log level: %s', conf['LOG_LEVEL'])
        logging.getLogger().setLevel(conf['LOG_LEVEL'])

    if not conf['DEBUG']:
        conf['DEBUG'] = False
    else:
        logging.warning('Debug-Mode is active')
        conf['DEBUG'] = True

    if not conf['TTS_URL']:
        conf['TTS_URL'] = "http://localhost/tts"
    if not conf['TWITCH_CHAT_PARENT']:
        conf['TWITCH_CHAT_PARENT'] = "localhost"

    if not conf['EXTDATA']:
        conf['EXTDATA'] = False
    else:
        logging.warning('Using external data source')
        conf['EXTDATA'] = True

    if not conf['DAYFLAT']:
        logging.warning('DAYFLAT is not set, using default')
        conf['DAYFLAT'] = False
    if not conf['IRC_CHANNEL']:
        logging.warning('IRC_CHANNEL is not set, disabling bot')
        conf['IRC_CHANNEL'] = False
    if not conf['IRC_COOLDOWN']:
        logging.warning('IRC_COOLDOWN is not set, using default')
        conf['IRC_COOLDOWN'] = 30
    if not conf['IRC_GREETER_WELCOME']:
        logging.warning('IRC_GREETER_WELCOME is not set, disabling greeter')
        conf['IRC_GREETER_WELCOME'] = False
    if not conf['IRC_GREETER_EMOTE']:
        logging.warning('IRC_GREETER_EMOTE is not set, using default')
        conf['IRC_GREETER_EMOTE'] = "HeyGuys"
    if not conf['IRC_BOT_EMOTE']:
        logging.warning('IRC_BOT_EMOTE is not set, using default')
        conf['IRC_BOT_EMOTE'] = "MrDestructoid"
    if not conf['IRC_BITRATE_MSG_PRE']:
        logging.warning('IRC_BITRATE_MSG_PRE is not set, using default')
        conf['IRC_BITRATE_MSG_PRE'] = "Current bitrate:"
    if not conf['IRC_BITRATE_MSG_SUF']:
        logging.warning('IRC_BITRATE_MSG_SUF is not set, using default')
        conf['IRC_BITRATE_MSG_SUF'] = "kBit/sec."
    if not conf['IRC_LOW_BITRATE_MSG_INTERVAL']:
        logging.warning('IRC_LOW_BITRATE_MSG_INTERVAL is not set, using default')
        conf['IRC_LOW_BITRATE_MSG_INTERVAL'] = 30

    if not conf['OBS_URL']:
        logging.warning('OBS_URL is not set, using default')
        conf['OBS_URL'] = "host.docker.internal"
    if not conf['OBS_PORT']:
        logging.warning('OBS_PORT is not set, using default')
        conf['OBS_PORT'] = "4444"
    if not conf['OBS_START_SCENE']:
        logging.warning('OBS_START_SCENE is not set, using default')
        conf['OBS_START_SCENE'] = 'START'
    if not conf['OBS_LIVE_SCENE']:
        logging.warning('OBS_LIVE_SCENE is not set, using default')
        conf['OBS_LIVE_SCENE'] = 'LIVE'
    if not conf['OBS_BRB_SCENE']:
        logging.warning('OBS_BRB_SCENE is not set, using default')
        conf['OBS_BRB_SCENE'] = 'BRB'
    if not conf['OBS_FORCED_BRB_SCENE']:
        logging.warning('OBS_FORCED_BRB_SCENE is not set, using default')
        conf['OBS_FORCED_BRB_SCENE'] = 'FORCED_BRB'
    if not conf['OBS_OFFLINE_SCENE']:
        logging.warning('OBS_OFFLINE_SCENE is not set, using default')
        conf['OBS_OFFLINE_SCENE'] = 'OFFLINE'

    if not conf['RTMP_STAT_URL']:
        logging.warning('RTMP_STAT_URL is not set, using default')
        conf['RTMP_STAT_URL'] = "http://localhost/stats/"
    if not conf['RTMP_STREAM_NAME']:
        logging.warning('RTMP_STREAM_NAME is not set, using default')
        conf['RTMP_STREAM_NAME'] = "live"
    if not conf['LOW_BITRATE']:
        logging.warning('LOW_BITRATE is not set, using default')
        conf['LOW_BITRATE'] = 500

    if conf['IRC_CHANNEL']:
        if not conf['IRC_USERNAME']:
            logging.critical('IRC_USERNAME is not set')
            raise Exception('Config Error')
        if not conf['IRC_OAUTH_TOKEN']:
            logging.critical('IRC_OAUTH_TOKEN is not set')
            raise Exception('Config Error')

    if not conf['OBS_PASSWORD']:
        logging.critical('OBS_PASSWORD is not set')
        raise Exception('Config Error')

    return conf

def main():
  conf = get_config()
  print(conf)

if __name__ == "__main__":
    main()
